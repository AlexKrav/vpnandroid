package com.vpn.client.ui.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.vpn.client.BaseActivity;
import com.vpn.client.R;
import com.vpn.client.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity implements MainActivityInterface {
    private MainActivityPresenter presenter;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main, null);
        setContentView(R.layout.activity_main);
        presenter = new MainActivityPresenter(this);
    }
}
