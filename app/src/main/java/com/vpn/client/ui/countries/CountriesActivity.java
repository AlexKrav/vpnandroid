package com.vpn.client.ui.countries;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.vpn.client.BaseActivity;
import com.vpn.client.R;
import com.vpn.client.databinding.ActivityCountriesBinding;

public class CountriesActivity extends BaseActivity implements CountriesActivityInterface {
    private ActivityCountriesBinding binding;
    private CountriesActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_countries, null);
        presenter = new CountriesActivityPresenter(this);
        setContentView(R.layout.activity_countries);
    }
}
